# * coding:utf-8 *
import sys
from textwrap import dedent
import re

from parsimonious import Grammar
from docopt import docopt

from jass_interpreter.jass_parser import JassParser
from jass_interpreter.python_generator import JassToPyTransformer

from code import InteractiveConsole

VERSION = "0.0.3"

__author__ = 'Joonas'

def remove_excess_empty_lines(src):
    # return src
    # remove empty lines
    src = re.sub(pattern="\s+\n", repl="\n", string=src)

    # insert newlines after larger blocks (function definitions, loop atm)
    src = re.sub(pattern="pass\n", repl="pass\n\n", string=src)

    return src

def j2p(src, parser=JassParser(), transformer=JassToPyTransformer()):
    return transformer.visit(parser.parse(src))

def main(options):
    files = options["<files>"]
    if options["--with-runtime"]:
        src_prefix = dedent('''
            # make sure the jass_runtime is always discoverable
            import sys
            sys.path.append("{mypath}")

            from jass_runtime.common_j import *
            from jass_runtime.blizzard_j import *
        ''').format(mypath='/'.join(__file__.split("/")[:-2]))
    else:
        src_prefix = ""

    src = src_prefix

    for fn in files:
        with open(fn) as jassfile:
            data = jassfile.read()
            src += j2p(data)

    if options["--code"]:
        src += j2p(options["--code"])

    if options["--interactive"]:
        raise RuntimeError("interactive mode not implemented")
    else:
        if options["--stdout"]:
            print(remove_excess_empty_lines(src))
        else:
            outfilename =  options["--out"] if options["--out"] else "out.py"
            with open(outfilename, "w") as pyfile:
                pyfile.write(remove_excess_empty_lines(src))



if __name__ == "__main__":
    docs = '''
        1.Read source from all listed <files>
        2.translate code to python 3.4 syntax
        3.set up imports etc..
        3.emit to file named by --out or "out.py" unless interactive mode
        4. if --interactive drop to interactive shell after executing previous code.

        Usage:
            jass_interpreter [--debug] [--with-runtime] [--interactive] [--code=<code>] [(--out=<out>|--stdout)] [<files>...]

        Options:
            -d, --debug         Emit debug statements
            --with-runtime      Mock the jass2 jass_runtime [default:False]
            -i, --interactive   Start an interactive session
            -h, --help          Show this help text
            -c, --code=<code>   code to append to the compiled source
            -o, --out=<out>     The file to write the output to,
                                    ignored in interactive mode

    '''
    options = docopt(docs, sys.argv[1:], version=VERSION)
    main(options)
