from jass_parser import JassParser, ascii_to_integer
import pytest
from python_generator import JassToPyTransformer

__author__ = 'Joonas'

@pytest.fixture(scope="function")
def jp():
    return JassParser()

@pytest.fixture(scope="function")
def nv():
    return JassToPyTransformer()


def test_name(jp, nv):
    assert str(nv.visit(jp.parse("name", root_node="name"))) == "name"

def test_typedef(jp, nv):
    assert str(nv.visit(jp.parse("type mytype extends builtintype", root_node="typedef"))) == "\nclass mytype(builtintype):\n    pass\n"

def test_float(jp, nv):
    assert nv.visit(jp.parse("1.", root_node="float_literal")) == 1.


def test_integer(jp, nv):
    assert nv.visit(jp.parse("1", root_node="integer_literal")) == 1
    assert nv.visit(jp.parse("1218178145", root_node="integer_literal")) == 1218178145


def test_integer_hex(jp, nv):
    assert nv.visit(jp.parse("0x93425715", root_node="integer_literal")) == 0x93425715


def test_string_literal(jp, nv):
    assert nv.visit(jp.parse('"218f1aertrew78g"', root_node="string_literal")) == '"218f1aertrew78g"'


def test_integer_ascii_1(jp, nv):
    assert nv.visit(jp.parse("'a'", root_node="integer_literal")) == ascii_to_integer('a')


def test_integer_ascii_4(jp, nv):
    assert nv.visit(jp.parse("'abcd'", root_node="integer_literal")) == ascii_to_integer('abcd')


def test_if_header(jp, nv):
    assert nv.visit(jp.parse("if 33 then\n", root_node="if_header")) == "if 33:"


def test_if_then(jp, nv):
    assert str(nv.visit(jp.parse("if 33 then\ncall Abrakadabra()\nendif\n", root_node="if_then"))) == "if 33:\n    Abrakadabra()"


def test_if_then2(jp, nv):
    assert (str(nv.visit(jp.parse("if 33 then\ncall Abrakadabra()\ncall a()\nendif\n", root_node="if_then"))) ==
            "if 33:\n    Abrakadabra()\n    a()")


def test_if_ifelse_else_complex(jp, nv):
    jass_src = ("if 33 then\n"
                "    call Abrakadabra()\n"
                "    call a()\n"
                "elseif 1==2 then\n"
                "    call b()\n"
                "elseif 1==3 then\n"
                "    call b()\n"
                "    call b()\n"
                "    call b()\n"
                "endif\n"
                )

    py_src = ("if 33:\n"
              "    Abrakadabra()\n"
              "    a()\n\n"
              "elif 1 == 2:\n"
              "    b()\n\n"
              "elif 1 == 3:\n"
              "    b()\n"
              "    b()\n"
              "    b()"
              )
    print(repr(str(nv.visit(jp.parse(jass_src, root_node="if_then")))))
    assert str(nv.visit(jp.parse(jass_src, root_node="if_then"))) == py_src


def test_call_expression(jp, nv):
    assert str(nv.visit(jp.parse("a()", root_node="call_expression"))) == "a()"


def test_expression__call(jp, nv):
    assert str(nv.visit(jp.parse("a()", root_node="expression"))) == "a()"


def test_call_statement(jp, nv):
    assert str(nv.visit(jp.parse("call a()", root_node="call_statement"))) == "a()"


def test_local_declaration_with_no_initial_value(jp, nv):
    assert str(nv.visit(jp.parse("local integer a  ", root_node="local_declaration"))) == "a = None"


def test_local_array_declaration(jp, nv):
    # all jass arrays are 8192 indicies
    assert str(nv.visit(jp.parse("local integer array a  ", root_node="local_declaration"))) == "a = [None]*8192"


def test_local_declaration_with_initial_value(jp, nv):
    assert str(nv.visit(jp.parse("local integer a = 33", root_node="local_declaration"))) == "a = 33"


def test_set_statement(jp, nv):
    assert str(nv.visit(jp.parse("set a = 33", root_node="statement"))) == "a = 33"


def test_statemen_call_statement(jp, nv):
    assert str(nv.visit(jp.parse("call a()\n", root_node="statement"))) == "a()"


def test_statement_return1(jp, nv):
    assert str(nv.visit(jp.parse("return a\n", root_node="statement"))) == "return a"


def test_statement_return2(jp, nv):
    assert str(nv.visit(jp.parse("return 1+22+31234234\n", root_node="statement"))) == "return 1 + 22 + 31234234"


def test_function_headers(jp, nv):
    jass = "function f takes nothing returns nothing\n"
    py = '\ndef f()->"nothing":'
    assert str(nv.visit(jp.parse(jass, root_node="function_headers"))) == py


def test_function_headers_1_arg(jp, nv):
    jass = "function f takes integer a returns boolean\n"
    py = '\ndef f(a:"integer")->"boolean":'
    assert str(nv.visit(jp.parse(jass, root_node="function_headers"))) == py

def test_function_headers_2_arg(jp, nv):
    jass = "function f takes integer a, integer b returns boolean\n"
    py = '\ndef f(a:"integer", b:"integer")->"boolean":'
    assert str(nv.visit(jp.parse(jass, root_node="function_headers"))) == py

def test_function_headers_3_arg(jp, nv):
    jass = "function f takes integer a, integer b, integer c returns boolean\n"
    py = '\ndef f(a:"integer", b:"integer", c:"integer")->"boolean":'
    assert str(nv.visit(jp.parse(jass, root_node="function_headers"))) == py

def test_function_headers_3_defined_args(jp, nv):
    jass = "function f takes integer a, integer b, integer c returns boolean\n"
    py = '\ndef f(a:"integer", b:"integer", c:"integer")->"boolean":'
    assert nv.visit(jp.parse(jass, root_node="function_headers")).declared_variables == ["a", "b", "c"]


def test_function_definition_pass(jp, nv):
    jass = "function f takes nothing returns nothing\nendfunction\n"
    py = '\ndef f()->"nothing":\n    \n    \n    pass\n'
    assert str(nv.visit(jp.parse(jass, root_node="function_definition"))) == py


def test_function_definition_pass2(jp, nv):
    # testing effect of empty lines
    jass = "function f takes nothing returns nothing\n\n    \nendfunction\n"
    py = '\ndef f()->"nothing":\n    \n    \n    pass\n'
    assert str(nv.visit(jp.parse(jass, root_node="function_definition"))) == py


def test_function_definition_simple(jp, nv):
    jass = "function f takes nothing returns nothing\ncall a()\nendfunction\n"
    py = '\ndef f()->"nothing":\n    \n    a()\n    pass\n'
    assert str(nv.visit(jp.parse(jass, root_node="function_definition"))) == py


def test_function_definition_complex(jp, nv):
    jass = "function f takes nothing returns nothing\nlocal integer zzz\ncall a()\ncall b(1,2,3)\nendfunction\n"
    py = '\ndef f()->"nothing":\n    \n    zzz = None\n    a()\n    b(1, 2, 3)\n    pass\n'
    assert str(nv.visit(jp.parse(jass, root_node="function_definition"))) == py


def test_function_definition_ref_globals(jp, nv):
    jass = "function f takes nothing returns nothing\nset myglobal = 33\nendfunction\n"
    py = '\ndef f()->"nothing":\n    global myglobal\n    myglobal = 33\n    pass\n'
    assert str(nv.visit(jp.parse(jass, root_node="function_definition"))) == py


def test_function_body(jp, nv):
    jass = "call a()\n"
    py = 'a()'
    assert str(nv.visit(jp.parse(jass, root_node="function_body"))) == py


def test_function_body_empty(jp, nv):
    jass = "\n"
    py = ''
    assert str(nv.visit(jp.parse(jass, root_node="function_body"))) == py


def test_loop_empty(jp, nv):
    jass = "loop\nendloop"
    py = 'while True:\n    \n    pass\n'
    assert str(nv.visit(jp.parse(jass, root_node="loop"))) == py


def test_function_dereference(jp, nv):
    jass = "function myfunc"
    py = 'myfunc'
    assert nv.visit(jp.parse(jass, root_node="function_dereference")) == py


def test_ascii_integer(jp, nv):
    jass = "'abcd'"
    py = 1633837924
    assert nv.visit(jp.parse(jass, root_node="integer_literal_ascii")) == py


def test_ascii_integer2(jp, nv):
    jass = "'abcd'"
    py = 1633837924
    assert nv.visit(jp.parse(jass, root_node="integer_literal")) == py


def test_ascii_integer3(jp, nv):
    jass = "'abcd'"
    py = '1633837924'
    assert str(nv.visit(jp.parse(jass, root_node="expression"))) == py


def test_ascii_integer4(jp, nv):
    jass = "set a = 'abcd'\n"
    py = 'a = 1633837924'
    assert str(nv.visit(jp.parse(jass, root_node="statement_block"))) == py


def test_ascii_integer5(jp, nv):
    jass = "globals\ninteger a = 'abcd' \nendglobals\n"
    py = 'a = 1633837924'
    assert str(nv.visit(jp.parse(jass, root_node="statement_block"))) == py



def test_bin_op_expr(jp, nv):
    jass = "a<b"
    py = 'a < b'
    assert nv.visit(jp.parse(jass, root_node="expression")) == py


def test_arglist(jp, nv):
    jass = "call b(true, true, false, false)"
    py = 'b(True, True, False, False)'
    assert str(nv.visit(jp.parse(jass, root_node="statement_block"))) == py


def test_statement_block_assignments(jp, nv):
    jass = "set a =33\nset b=false\nset c=333"
    py = {"a", "b", "c"}
    assert nv.visit(jp.parse(jass, root_node="statement_block")).assigned_variables == py


def test_statement_block_declarations_and_assignments(jp, nv):
    jass = "local integer a =33\nset b=false\nset c=333\nlocal specialrandomthing d\n  \n//useless scrap\n"
    node = nv.visit(jp.parse(jass, root_node="statement_block"))
    assert node.assigned_variables == {"a", "b", "c"}
    assert node.declared_variables == {"a", "d"}


def test_function_declared_variables(jp, nv):
    jass = "function f takes integer a, integer b returns nothing\n" \
           "local integer d\n" \
           "endfunction\n"
    node = nv.visit(jp.parse(jass, root_node="function_definition"))
    assert node.declared_variables == {"a", "b", "d"}

def test_function_declared_variables_and_assigned_variables(jp, nv):
    jass = "function f takes integer a, integer b returns nothing\n" \
           "local integer d = 33\n" \
           "local integer x\n" \
           "set mynonlocal = d\n" \
           "endfunction\n"
    node = nv.visit(jp.parse(jass, root_node="function_definition"))
    assert node.declared_variables == {"a", "b", "d", "x"}
    assert node.assigned_variables == {"a", "b", "d", "mynonlocal"}
    assert node.nonlocals == {"mynonlocal"}

def test_function_declared_variables_and_assigned_variables_empty(jp, nv):
    jass = "function f takes nothing returns nothing\n" \
           "endfunction\n"
    node = nv.visit(jp.parse(jass, root_node="function_definition"))
    assert node.declared_variables == set()
    assert node.assigned_variables == set()
    assert node.nonlocals == set()
