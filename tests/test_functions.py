from python_generator import indented

__author__ = 'Joonas'

from jass_parser import ascii_to_integer

def test_ascii_to_integer_short():
    assert ascii_to_integer(b'a') == 97

def test_ascii_to_integer_long_simple():
    assert ascii_to_integer(b'\0\0\0a') == 97
    assert ascii_to_integer(b'\0\0a\0') == 97 << 8
    assert ascii_to_integer(b'\0a\0\0') == 97 << 16
    assert ascii_to_integer(b'a\0\0\0') == 97 << 24


def test_ascii_to_integer_works_if_source_is_unicode():
    assert ascii_to_integer(u'a\0\0\0') == 97 << 24


def test_indented_1():
    s1 = """abcd"""
    s2 = """    abcd"""
    assert indented(s1) == s2


def test_indented_2():
    s1 = """abcd
      ikiiikikikikiki sasdf 248148 5184874 54814 845 48 4 -023423059096236+
    """
    s2 = """    abcd
          ikiiikikikikiki sasdf 248148 5184874 54814 845 48 4 -023423059096236+
        """
    assert indented(s1) == s2






