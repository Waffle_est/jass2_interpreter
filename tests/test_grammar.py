from jass_parser import JassParser
import pytest

__author__ = 'Joonas'

@pytest.fixture(scope="function")
def jp():
    return JassParser()


def test_name(jp):
    assert jp.parse("name",root_node="name").text == "name"
    assert jp.parse("myvEry_longAndstrangename",root_node="name").text == "myvEry_longAndstrangename"

def test_typedef(jp):
    assert jp.parse("type mytype extends builtintype",root_node="typedef").text == "type mytype extends builtintype"

def test_float(jp):
    assert jp.parse("11.323134",root_node="float_literal").text == "11.323134"
    assert jp.parse("11.", root_node="float_literal").text == "11."
    assert jp.parse("11.", root_node="number").text == "11."
    assert jp.parse("11.  ", root_node="number").text == "11.  "


def test_integer(jp):
    assert jp.parse("1",root_node="integer_literal").text == "1"
    assert jp.parse("1151815684841582", root_node="integer_literal").text == "1151815684841582"
    assert jp.parse("11  ", root_node="number").text == "11  "


def test_integer_hex(jp):
    assert jp.parse("0x1", root_node="integer_literal_hex").text == "0x1"


def test_integer_ascii(jp):
    assert jp.parse("'1'", root_node="integer_literal_ascii").text == "'1'"


def test_single_newline(jp):
    assert jp.parse("\n", root_node="newline").text == "\n"


def test_multiple_newline(jp):
    assert jp.parse("\n\n\n\n", root_node="newline").text == "\n\n\n\n"


def test_un_op(jp):
    assert jp.parse("not",root_node="un_op").text == "not"


def test_bin_op(jp):
    assert jp.parse("*", root_node="bin_op").text == "*"
    assert jp.parse("/", root_node="bin_op").text == "/"
    assert jp.parse("+", root_node="bin_op").text == "+"
    assert jp.parse("-", root_node="bin_op").text == "-"
    assert jp.parse("- \t\t  \t", root_node="bin_op").text == "- \t\t  \t"


def test_whitespace(jp):
    assert jp.parse("", root_node="_").text == ""
    assert jp.parse(" ", root_node="_").text == " "
    assert jp.parse("  \t ", root_node="_").text == "  \t "


def test_unart_operation(jp):
    assert jp.parse("not 1", root_node="unary_operation").text == "not 1"



def test_expression(jp):
    assert jp.parse("11.323134", root_node="expression").text == "11.323134"
    assert jp.parse("11.", root_node="expression").text == "11."
    assert jp.parse("11", root_node="expression").text == "11"
    assert jp.parse("(  11 )  ", root_node="expression").text == "(  11 )  "


def test_complex_expression_with_unary_opearation(jp):
    assert jp.parse("not (not (  11 ) ) ", root_node="expression").text == "not (not (  11 ) ) "


def test_complex_expression_with_binary_opearation(jp):
    assert jp.parse("1*1*1*2*3*4", root_node="expression").text == "1*1*1*2*3*4"
    assert jp.parse("( 1*1 *1)*2  \t* 3*(not 4)", root_node="expression").text == "( 1*1 *1)*2  \t* 3*(not 4)"
    assert jp.parse("(  11 ) *(  11 )  ", root_node="expression").text == "(  11 ) *(  11 )  "


def test_statement_local_with_initial_value(jp):
    # statements have optional whitre space at start of line
    assert jp.parse("local integer myvar=3218128\n", root_node="statement").text == "local integer myvar=3218128\n"


def test_statement_local(jp):
    # statements have optional whitre space at start of line
    assert jp.parse("local integer myvar\n", root_node="statement").text == "local integer myvar\n"
    assert jp.parse("local real myvar\n", root_node="statement").text == "local real myvar\n"
    assert jp.parse("local boolean myvar\n", root_node="statement").text == "local boolean myvar\n"


def test_statement_set(jp):
    # statements have optional whitre space at start of line
    assert jp.parse("set myvar = 33\n", root_node="statement").text == "set myvar = 33\n"


def test_statement_call_no_args(jp):
    assert jp.parse("call myfunc()\n", root_node="statement").text == "call myfunc()\n"


def test_statement_call_no_1_arg(jp):
    assert jp.parse("call myfunc(1)\n", root_node="statement").text == "call myfunc(1)\n"


def test_statement_call_no_3_args(jp):
    assert jp.parse("call myfunc(1,2,3)\n", root_node="statement").text == "call myfunc(1,2,3)\n"


def test_statement_call_complex(jp):
    # a very abusive case.. if this works it should all work
    assert jp.parse("call myfunc((1),2,a(b,(((c))),d),2+2)\n", root_node="statement").text == "call myfunc((1),2,a(b,(((c))),d),2+2)\n"


def test_string_literal(jp):
    assert jp.parse('"abcd"', root_node="string_literal").text == '"abcd"'
    assert jp.parse('""', root_node="string_literal").text == '""'
    assert jp.parse('"abcd5481514!?#¤#%)¤!#&¤("', root_node="string_literal").text == '"abcd5481514!?#¤#%)¤!#&¤("'
    #TODO: handle quote escaping!


def test_type_name(jp):
    assert jp.parse("string", root_node="type_name").text == "string"
    assert jp.parse("integer", root_node="type_name").text == "integer"
    assert jp.parse("real", root_node="type_name").text == "real"
    assert jp.parse("boolean", root_node="type_name").text == "boolean"


def test_typed_var(jp):
    assert jp.parse("string myvar", root_node="typed_var").text == "string myvar"
    assert jp.parse("integer myvar", root_node="typed_var").text == "integer myvar"
    assert jp.parse("real myvar", root_node="typed_var").text == "real myvar"
    assert jp.parse("boolean myvar", root_node="typed_var").text == "boolean myvar"


def test_local_declaration(jp):
    assert jp.parse("local integer myvar", root_node="local_declaration").text == "local integer myvar"
    assert jp.parse("local real myvar", root_node="local_declaration").text == "local real myvar"
    assert jp.parse("local boolean myvar", root_node="local_declaration").text == "local boolean myvar"
    assert jp.parse("local boolean array myvar", root_node="local_declaration").text == "local boolean array myvar"


def test_argument_list(jp):
    assert jp.parse("string name1", root_node="argument_list").text == "string name1"
    assert jp.parse("string name1,string name2", root_node="argument_list").text == "string name1,string name2"
    assert jp.parse("string name1   ,  string name2", root_node="argument_list").text == "string name1   ,  string name2"


def test_function_headers(jp):
    assert (jp.parse("function myfuncname takes string name1   ,  string name2 returns string\n", root_node="function_headers").text ==
            "function myfuncname takes string name1   ,  string name2 returns string\n")
    assert (jp.parse("function myfuncname takes nothing returns nothing\n", root_node="function_headers").text ==
            "function myfuncname takes nothing returns nothing\n")
    assert (jp.parse("function myfuncname takes integer i1, integer i2, string s1, boolean b1 returns nothing\n", root_node="function_headers").text ==
            "function myfuncname takes integer i1, integer i2, string s1, boolean b1 returns nothing\n")


def test_return_statement(jp):
    assert jp.parse("return ", root_node="return_statement").text == "return "
    assert jp.parse("return abrakadabra", root_node="return_statement").text == "return abrakadabra"


def test_function_end(jp):
    assert (jp.parse("     \t\t endfunction \t\t \n", root_node="function_end").text ==
            "     \t\t endfunction \t\t \n")


def test_function_definition(jp):
    src = """     function myfunction takes nothing returns nothing

         endfunction\n"""
    assert jp.parse(src, root_node="function_definition").text == src

    src = (
    """ function myfunction takes string s, string s2 returns integer
        local integer aab
        return aab
    endfunction\n""")
    assert jp.parse(src, root_node="function_definition").text == src

    src = (
    """ function myfunction takes string s, string s2 returns integer
        local integer aab
        local integer bbb

        return aab*bbb
    endfunction\n""")
    assert jp.parse(src, root_node="function_definition").text == src


def test_if_end(jp):
    assert jp.parse("endif\n", root_node="if_end").text == "endif\n"


def test_if_header(jp):
    assert jp.parse("if 11 then\n", root_node="if_header").text == "if 11 then\n"


# def test_if_else(jp):
#     assert jp.parse("else\n", root_node="if_else").text == "else\n"


def test_if_then(jp):
    src = """if 11 then

        endif
"""
    assert jp.parse(src, root_node="if_then").text == src



def test_if_elseif_else_complex(jp):
    src = """if 11 then

        elseif 22 then

        elseif 77 then

        elseif a==b+33 then

        else

        endif
"""
    assert jp.parse(src, root_node="if_then").text == src

def test_array_dereference(jp):
    assert jp.parse("a[33]", root_node="array_dereference").text == "a[33]"
    assert jp.parse("a[33]", root_node="name").text == "a[33]"


def test_array_dereference_repeated(jp):
    assert jp.parse("a[33][123]", root_node="name").text == "a[33][123]"


def test_statement_with_comments(jp):
    assert jp.parse("call a()   //\n", root_node="statement").text == "call a()   //\n"
    assert jp.parse("call a()   //a\n", root_node="statement").text == "call a()   //a\n"
    assert jp.parse("call a()   //abcd\n", root_node="statement").text == "call a()   //abcd\n"
    assert jp.parse("call a()   // a  a  a a dfs sf sw 213 213 \n", root_node="statement").text == "call a()   // a  a  a a dfs sf sw 213 213 \n"
    assert jp.parse("call a()   // *?½'\"?=13 \n", root_node="statement").text == "call a()   // *?½'\"?=13 \n"


def test_expression_with_integer_literal_and_call_1(jp):
    expr = '1 + a(1)'
    assert jp.parse(expr, root_node="expression").text == expr


def test_expression_with_string_literal_and_call_2(jp):
    expr = '"a" + a(1)'
    assert jp.parse(expr, root_node="expression").text == expr

def test_string_literals(jp):
    jass = '''"    abc  "'''

def test_escaped_string_literals(jp):
    jass = '''"    \"  "'''
    assert jp.parse(jass, root_node="string_literal").text == jass

def test_escaped_string_literals2(jp):
    jass = '''"    \"\"\"\" \" \\ \\\\  "'''
    assert jp.parse(jass, root_node="string_literal").text == jass
